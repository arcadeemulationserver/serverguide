local S = minetest.get_translator("serverguide")

guideBooks.Common.register_guideBook("serverguide:guide",	{
		inventory_image="serverguide_inv.png",
		description_short=minetest.colorize("#ffffff", S("Server Guide")),
		description_long=minetest.colorize("#888888", S("don't feel lost!")),
		droppable = false,
		style={

			cover={
				bg = "cover.png",
				next = "next.png",
				w = 5,
				h = 8,
			},
			page={
				bg = "bg.png",
				next = "next.png",
				prev = "prev.png",
				start = "start.png",
				w = 10,
				h = 8,
        textcolor = "gray"
			},
			buttonGeneric = "generic.png",
		},
		ptype = false

})



--------------------------------------------------------------------------------
--													   Introduction																		--
--------------------------------------------------------------------------------

guideBooks.Common.register_section("serverguide:guide",	S("Introduction"), {
		description = S("Introduction")
})


guideBooks.Common.register_page("serverguide:guide", S("Introduction"), 1, {
	text1=
		S("HOW TO PLAY") .. "\n\n" ..

		S("Look for the specific signs in order to enter a minigame, and left-click them to join the match (or the queue).") .. "\n\n" ..

		S("Some minigames allow you to enter an ongoing match (i.e. QuikBild), while as for others you'll have to wait the end of the match (i.e. Sumo).") .. "\n\n\n" ..

		S("LEAVE A GAME") .. "\n\n" ..

		S("In order to leave any minigame and return back to the lobby, just open the chat and do /quit"),

    extra="image[6,0.75;4,2.9;serverguide_intro_sign.png]"
})

--------------------------------------------------------------------------------
--													    Minigames																			--
--------------------------------------------------------------------------------

guideBooks.Common.register_section("serverguide:guide",	S("Minigames"), {
		description = S("Minigames"),
		master = true
})

--Inspired by eggwars, but with a unique twist. Defend your team's great crystal. Without it, you cannot respawn when you die. Venture around the map to uncover gem mines. Protect your color's gems, since weapons made with them are your team's greatest weakness. Capture the gems of other teams and find the shops to sell them for weapons which are most effective against that color team!. Swords used against players of that color add 3 hp to the damage, while picks of the same color dig gem blocks and great gems faster. Play defense by surrounding your great gem with gem blocks, which are difficult to destroy. In the 2 team version, ruby vies against emerald, and can use opal and sapphire to their advantage. In the 4 team version, all four gem types are backed by a team.


guideBooks.Common.register_section("serverguide:guide",	"Gems", {
		description = "Gems",
		slave = S("Minigames"),
})

guideBooks.Common.register_page("serverguide:guide", "Gems", 1, {
	text1=
      S("Inspired by eggwars, but with a unique twist. Defend your team's great crystal. Without it, you cannot respawn when you die. Venture around the map to uncover gem mines. Protect your color's gems, since weapons made with them are your team's greatest weakness. Capture the gems of other teams and find the shops to sell them for weapons which are most effective against that color team!. Swords used against players of that color add 3 hp to the damage, while picks of the same color dig gem blocks and great gems faster. Play defense by surrounding your great gem with gem blocks, which are difficult to destroy. In the 2 team version, ruby vies against emerald, and can use opal and sapphire to their advantage. In the 4 team version, all four gem types are backed by a team.")

})


-- guideBooks.Common.register_section("serverguide:guide",	"Block League", {
-- 		description = "Block League",
-- 		slave = S("Minigames"),
-- })

-- guideBooks.Common.register_page("serverguide:guide", "Block League", 1, {
-- 	text1=
--       S("Inspired by the online game S4 League, Block League is a team hack'n'slash where alertness and strategy are everything. Choose between Touchdown and Deathmatch mode, customise your weapons set and jump into the action!") .. "\n\n\n" ..

--       "TOUCHDOWN" .. "\n" ..
--       S("Get the ball and bring it into the enemy base. The first team reaching 10 points wins") .. "\n\n" ..

--       "DEATHMATCH" .. "\n" ..
--       S("The first team reaching the kill cap wins"),

--     text2=

-- 	    S("WEAPONS") .. "\n\n\n" ..

-- 	    S("Submachine gun: automatic pistol perfectly suited for close range bursts") .. "\n\n\n" ..
-- 	    S("2H Sword: push away enemies by left-clicking and dash forward by right-clicking. The latter temporarily slows down after the execution") .. "\n\n\n" ..
-- 			S("Pixelgun: instant shot, oneshot weapon, perfect for sniping") .."\n\n\n"..
-- 			--S("Rocket launcher: shoots a rocket blowing up on contact, causing AoE damage up to 4 blocks. Damage decreases as the distance from the point of impact increases"),
-- 			S("(more coming soon)") .. "\n\n\n\n" ..
-- 			S("Bouncer: (left click) makes you bounce against the walls. (right click) Side dash. Both actions require 20 energy (see next page)"),

--     extra="image[5,1.15;1,1;serverguide_blockleague_smg.png]" ..
--           "image[5,2.4;1,1;serverguide_blockleague_sword.png]" ..
-- 					"image[5,3.7;1,1;serverguide_blockleague_pixelgun.png]" ..
-- 					"image[5,5.85;1,1;serverguide_blockleague_bouncer.png]"
-- })

-- guideBooks.Common.register_page("serverguide:guide", "Block League", 2, {
-- 	text1=
-- 		S("ENERGY") .. "\n\n\n\n" ..
-- 		S("The energy is that yellow bar next to the life bar. Every player has 100 energy points and it allows to move fast and perform tricks with the Bouncer, on the condition players have enough.") .. "\n\n" ..
-- 		S("There are two ways to consume energy: the first one are the aformentioned tricks, whereas the second one is to hold the ball in Touchdown mode, which will quickly drain energy out, stopping it from regenerating"),

-- 		extra="image[0.2,0.95;5.2,0.4;serverguide_blockleague_energy.png]"
-- })


-- guideBooks.Common.register_section("serverguide:guide",	"Murder",	{
-- 		description = "Murder",
-- 		slave = S("Minigames"),
-- })

-- guideBooks.Common.register_page("serverguide:guide", "Murder", 1, {
-- 	text1=
-- 		S("When the match starts, a murderer is randomly selected, whereas other players are all detectives. Who is who? Trust no one!") .. "\n\n\n\n" ..

-- 		S("Murderer: use the knife to eliminate other players, supported by a kit of items to confound them") .. "\n\n\n\n" ..
-- 		S("Detective: your gun is the only thing that can stop the murderer, but beware: shooting another detective will result in eliminating both of you!") .. "\n\n\n\n" ..

-- 		S("VICTORY CONDITIONS") .. "\n\n" ..
-- 		S("Murderer: kill all the players within the time limit") .. "\n" ..
-- 		S("Detectives: kill the murderer or survive until the end"),

-- text2=
-- 		S("MURDERER KIT") .. "\n\n\n" ..

-- 		S("Knife: left click to stab, right click to throw. Their only weapon") .. "\n\n" ..
-- 		S("Blindness: (single-use) every detective gets blinded for 3 seconds, being unable to see anything") .. "\n\n" ..
-- 		S("Skin shuffler: (single-use) change the skins of every player still in game") .. "\n\n" ..
-- 		S("Locator: (single-use) reveals the position of the closest player") .. "\n\n\n\n" ..
-- 		S("BEWARE: when the murderer kills a detective, for 4 seconds all the players will be able to see where the murder took place. Better be sneaky!"),

-- 	extra="image[3.9,2.2;1,1;serverguide_murder_knife.png]" ..
-- 				"image[3.9,3.8;1,1;serverguide_murder_gun.png]" ..
-- 				"image[5.38,1.3;0.6,0.6;serverguide_murder_knife2.png]" ..
-- 				"image[5.38,2.2;0.6,0.6;serverguide_murder_blindness.png]" ..
-- 				"image[5.38,3.1;0.6,0.6;serverguide_murder_skinshuffler.png]" ..
-- 				"image[5.38,3.85;0.6,0.6;serverguide_murder_locator.png]"
-- })


guideBooks.Common.register_section("serverguide:guide",	"Parkour", {
		description = "Parkour",
		slave = S("Minigames"),
})

guideBooks.Common.register_page("serverguide:guide", "Parkour", 1, {
	text1=
			S("There are a few parkour places scattered around the lobby, where it's possible for you to showcase your climbing skills in exchange for some rewards") .. "\n\n\n" ..

			S("The Tower is the first one you'll encounter, a real blessing to discover the thrill of the sneak glitch: ") ..
			S("jump, press 'sneak' before colliding against the edge of a block, and you'll see your character make a little jump that will allow you to reach farther distances") .. "\n\n\n" ..

			S("Where are the other parkour places? Well... it'd be too easy to tell you!"),

    extra="image[6,0.75;4,2.5;serverguide_parkour.png]"
})


guideBooks.Common.register_section("serverguide:guide",	"Skywars",	{
		description = "Skywars",
		slave = S("Minigames"),
})

guideBooks.Common.register_page("serverguide:guide", "Skywars", 1, {
	text1=
	  	S("Fight, dig and build in a battle to the death across the skies. And beware not to fall, or you'll be eliminated!") .. "\n\n\n" ..

		S("Each player starts from a small island containing a few initial treasures, with intent to improve their equipment and go on the offensive. ") ..
		S("At the center of every map then, there is a large island which conceals more refined treasures, goal for everyone who longs for a stronger arsenal")
})


guideBooks.Common.register_section("serverguide:guide",	"Sumo", {
		description = "Sumo",
		slave = S("Minigames"),
})

guideBooks.Common.register_page("serverguide:guide", "Sumo", 1, {
	text1=
		S("Like the Japanese national sport of the same name, win by throwing your adversaries off the platform. Or in this case... into the water!") .. "\n\n\n" ..

		S("Push other players with the left mouse button - but be careful! You only have three lives. Avoid your opponent's hits by vaulting with the right button and confuse them by switching positions with left click and sneak")
})


guideBooks.Common.register_section("serverguide:guide",	"Wormball", {
		description = "Wormball",
		slave = S("Minigames"),
})

guideBooks.Common.register_page("serverguide:guide", "Wormball", 1, {
	text1=
		S("In this take on the classic, the snake becomes a worm and the action takes place in 3D: choose whether you want to play solo or take on other players.") .. "\n\n\n" ..

		S("Guide your worm towards the food, as with each bite it becomes bigger, and so does your highscore, but beware: colliding with walls, other players or your own body will result in an instant game over. And if you don't want to be penalised, make sure not to eat the food in your colour!")
})








guideBooks.Common.register_section("serverguide:guide",	"TNTrun!", {
		description = "TNTrun!",
		slave = S("Minigames"),
})

guideBooks.Common.register_page("serverguide:guide", "TNTrun!", 1, {
	text1=
		S("Run and don't look down! Keep other players on their toes by knocking the ground out from under them with your torch. The ground can fall out from under you, too, so watch out! ") .. "\n\n\n"

		
})



guideBooks.Common.register_section("serverguide:guide",	"QuikBild", {
		description = "QuikBild",
		slave = S("Minigames"),
})

guideBooks.Common.register_page("serverguide:guide", "QuikBild", 1, {
	text1=
		S("A Pictionary(tm) clone. Each person gets to have a turn to be the Artist. The artist is given a word and then they must build a representation of it. The other players try to guess what the Artist is building. The first player to guess the word gets a point, and the Artist gets a point if their word is guessed before the time is up. Rules: Do not build letters or words, that ruins the game. Multiple people can win the game, and winners are announced at the end.") .. "\n\n\n"
})
		


--------------------------------------------------------------------------------
--													       Parties  		   														--
--------------------------------------------------------------------------------

guideBooks.Common.register_section("serverguide:guide", S("Parties"), {
	description = S("Parties")
})

guideBooks.Common.register_page("serverguide:guide", S("Parties"), 1, {
	text1=
		S("WANT TO PLAY IN COMPANY? USE PARTIES!") .. "\n\n" ..

		S("When you're in a party, only the party leader can enter a game, and when they do, the rest of the group is automatically added.") .. "\n\n" ..

		S("This is great to play with friends, as you'll all be put in the same team (if the map supports teams).") .. "\n\n" ..

		S("Finally, parties also have a separate chat, only visible to their members."),

	text2=
		S("COMMANDS") .. "\n\n" ..
		S("/party invite <player>: invites a player into the party") .. "\n" ..
		S("/party join: accepts the invite to a party") .. "\n" ..
		S("/party leave: leaves the party") .. "\n" ..
		S("/party disband: disbands the party (only the party leader can do that)") .. "\n\n" ..

		S("/p <message>: sends a message in the party chat")
})


--------------------------------------------------------------------------------
--													        Rules  																		--
--------------------------------------------------------------------------------

guideBooks.Common.register_section("serverguide:guide", S("Rules"), {
	description = S("Server Rules")
})

guideBooks.Common.register_page("serverguide:guide", S("Rules"), 1, {
	text1=
		S("1) Always respect other players") .. "\n" ..
		S("2) No excessive swearing") .. "\n" ..
		S("3) No modified clients") .. "\n\n" ..

		S("Admins reserve the right to remove whoever transgress the rules. In case of repeated misbehaviours, the user will be banned")
})


-- --------------------------------------------------------------------------------
-- --													     Mod																					--
-- --------------------------------------------------------------------------------

-- guideBooks.Common.register_section("serverguide:guide",	S("Mods"), {
-- 		description=S("Mods"),
-- })

-- guideBooks.Common.register_page("serverguide:guide", S("Mods"),	1, {
-- 		text1=
-- 			S("Did you know that most of the mods here in the server have been made by us? And they're all free software!") .. "\n\n" ..
-- 			S("Our goal is to make them lighter and well-performing, yet at the same time solid and funny: look them up on gitlab.com/zughy-friends-minetest to know more about it"),

-- 		text2=
-- 			S("SERVER MODS") .. "\n\n" ..

-- 			S("Developed by us:") .. "\n\n" ..

-- 			[[
-- 			Achievements_lib
-- 			Arena_lib
-- 			Block League
-- 			Enderpearl
-- 			Hub manager
-- 			Magic Compass
-- 			Murder
-- 			Panel_lib
-- 			Parties
-- 			Skins Collectible
-- 			Skywars
-- 			Whitelist
-- 			]]
-- 			.."\n"..
-- 			S("Server specific mods:") .. "\n\n" ..
-- 			[[
-- 			Decoblocks
-- 			Server Guide
-- 			Server Manager
-- 			Suggestion Box
-- 			Terms of Service
-- 			]]
-- })

-- guideBooks.Common.register_page("serverguide:guide", S("Mods"),	2, {
-- 	text1=
-- 	S("Not developed by us: all the other mods listed by doing /mods")
-- })

--------------------------------------------------------------------------------
--													        Info   																		--
--------------------------------------------------------------------------------

guideBooks.Common.register_section("serverguide:guide",S("Info"), {
	description=S("Server Info")
})

guideBooks.Common.register_page("serverguide:guide", S("Info"), 1, {
	text1=
		S("A.E.S., which stands for Arcade Emulation System, is a server made by a group of guys in their free time, currently geolocalised in Italian and English.") .. "\n\n" ..

		S("It's one of the projects of Etica Digitale (an Italian group divulging about digial ethics) and it's born as a place where to play and have fun, in the full respect of privacy and human rights."),

-- 	text2=
-- 	  S("WHAT DO YOU MEAN?") .. "\n\n" ..
-- 		S("PRIVACY") .. "\n\n" ..

-- 		S("Do you know those ads which are so invasive that sometimes it almost feels like they're spying on you? There. ") ..
-- 		S("This practice of following the user from the ground up is actually older than we think, and it was originated in the year 2000, ") ..
-- 		S("after Google decided to survive an economic bubble by collecting more data than what it actually needed. ") ..
-- 		S("Though it promised this was only going to be temporary, Google didn't change this behaviour, and on the contrary, more and more companies (including government enterprises) ") ..
-- 		S("started doing the same because... well, there was no law to prohibit it. ") ..
-- 		S("Facebook, Amazon, Microsoft, even some network providers like Verizon (today's Oath) saw it as a golden goose, ") ..
-- 		S("and it didn't really matter that this would have led to see people not as human being anymore, but as numbers.")
})

-- guideBooks.Common.register_page("serverguide:guide", S("Info"), 2, {
-- 	text1=
-- 		S("Today's situation has become so paradoxical that even those online newspapers that do address the issue, have services like Google Analytics running in background to collect new data. ") ..
-- 		S("And even schools, though being a public envronment, use services provided by these companies - Teams, Meet, Classroom, Zoom etc.") .. "\n\n" ..

-- 		S("The world of videogames is no exception, 2020 has indeed reached a new level of monitoring: ") ..
-- 		S("Valorant, a game by Riot Games, installs an anticheat which, to assure no one is cheating, automatically starts as the computer is turned on. ") ..
-- 		S("The anticheat, which has been shown to be crossable anyways, has COMPLETE ACCESS to the computer, so much that it falls under the definition of 'rootkit': ") ..
-- 		S("a set of tools used to hack, which allows to gain the complete control of the infected PC."),

-- 	text2=
-- 	  S("WHAT WE HAVE DONE") .. "\n\n" ..
-- 		S("A.E.S. runs on an engine (Minetest) which doesn't dispose of an actual account: ") ..
-- 		S("this means that your data aren't stored in a central server held by some company (as opposed to MineCRAFT which has them held by Microsoft) and your emails aren't associated to any account. ") ..
-- 		S("In other words, we couldn't steal your data even if we wanted to.") .. "\n\n" ..

-- 		S("On the other hand, as far as transparency is concerned, each mod of the server is free software: ") ..
-- 		S("as for the security level, this means that anyone who knows about it, can analyse the code of the mod and tell you if it's actually doing what it says (and it comes with other perks too!).") .. "\n\n" ..

-- 		S("As for the anticheat: adopting such tools like Valorant did, must never be the solution. The user musn't be spied. If something, bypassed")
-- })


-- guideBooks.Common.register_page("serverguide:guide", S("Info"), 3, {
-- 	text1=
-- 		S("HUMAN RIGHTS") .. "\n\n" ..

-- 		S("Nowadays we condemn events like the Holocaust and the millions of deaths it has brutally caused. ") ..
-- 		S("Information channels and politics, however, though repeting phrases like 'never again', seem to ignore what's happening in one of today's biggest economic powers in the world, which has little to envy to the Holocaust: China. ") ..
-- 		S("For the past 70 years, the Chinese Communist Party has used concentration camps (laojiao), censorship, and extreme levels of surveillance, to nip any voice of dissent in the bud, without any trace of respect for human rights. ") ..
-- 		S("Xinjiang region is compared to a digital gym-jail, Tibet doesn't allow indipendent visitors since 2008, internet is completely controlled and the last events in Hong Kong speak for themselves."),

--   text2=
-- 	  	S("This situation makes the life of Chinese citizens and minorities in China - like the Uyghur one - resemble that of working animals, and in no way this should be tolerated. ") ..
-- 		S("Many Western companies though, don't seem to be very sensitive about it and, to make money, they are willing to look the other way.") .. "\n\n" ..

-- 		S("And again, the world of videogames is not an exception: in 2015 the American company Riot Games has passed under the management of Chinese company Tencent, ") ..
-- 		S("while in 2019 Blizzard had no problem suspending a player from Hong Kong for supporting his country during a tournament, thus messing with the company's earnings in China.") .. "\n\n" ..

-- 		S("If a Chinese citizen isn't given much choice on how to behave, the same doesn't apply to Western companies. And it's only fair to let it be known what position did certain companies decide to take. ") ..
-- 		S("Without going around and around with it, shopping on these games is equivalent to economically support a dictatorship.")

-- })


-- guideBooks.Common.register_page("serverguide:guide", S("Info"), 4, {
-- 	text1=
-- 		S("WHAT WE HAVE DONE") .. "\n\n" ..

-- 		S("We said: enough. A.E.S. was born also to give space and tools to those who don't want to support certain realities. ") ..
-- 		S("The road is long and narrow, but it doesn't matter: we won't submit to this inhumanity.") .. "\n\n" ..
-- 		S("Games like Minetest take power and authority away from these companies, because they are created by single people, hence they can be personalised and run on each person's homemade server. ") .. "\n\n" ..
-- 		S("There's no need to come to terms with monsters anymore.")

-- })
